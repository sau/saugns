.\" Copyright (c) 2019-2025 Joel K. Pettersson
.\"
.\" This file is licensed under Creative Commons Attribution-ShareAlike 4.0
.\" <https://creativecommons.org/licenses/by-sa/4.0/>.
.Dd January 4, 2025
.Dt SAUGNS 1
.Os
.Sh NAME
.Nm saugns
.Nd Scriptable AUdio GeNeration System
.Sh SYNOPSIS
.Nm saugns
.Op Fl a | m
.Op Fl r Ar srate
.Op Fl \-mono
.Op Fl o Ar file
.Op Fl \-stdout
.Op Fl d
.Op Fl p
.Op Ar variable\| Ns Cm \&= Ns Ar value
.Op Fl e
.Ar script ...
.Nm saugns
.Fl c
.Op Fl d
.Op Fl p
.Op Ar variable\| Ns Cm \&= Ns Ar value
.Op Fl e
.Ar script ...
.Sh DESCRIPTION
.Nm
is an audio generation program.
Written in C, it implements the SAU (Scriptable AUdio) language \-
a simple language for mathematical audio synthesis.
.Pp
The program reads SAU (Scriptable AUdio) files by default,
or passed strings if the
.Fl e
option is used.
Output is by default to system audio, but may instead be muted and/or
written to a 16-bit PCM WAV file; it is also possible to stream audio
data to stdout, either raw (--stdout) or AU (-o -).
.Pp
Scripts can use an arbitrary number of audio generators of the types supported,
which can also be connected for modulation.
.Bl -tag -width Ds
.It Vt A
Amplitude generator, makes sweepable amplitude offsets i.e. DC offsets.
.It Vt N
Noise generator, makes plain unpitched noises of chosen color and distribution.
.It Vt R
Rumble oscillator (a.k.a. random line segments oscillator), produces smooth,
pitched noises with any of a variety of modes and line types.
.It Vt W
Wave oscillator, produces tones with a chosen waveshape.
.El
.Pp
Synthesis techniques supported include FM/PM, AM/RM, and some varieties of
PD (phase distortion) synthesis as well as pulsar synthesis.
Duration and some other attributes can be calculated for each script
without generating audio; the scripting language is not Turing complete.
.Sh OPTIONS
By default, system audio output is enabled.
.Bl -tag -width Ds
.It Fl a
Audible; always enable system audio output.
.It Fl c
Check scripts only; parse, handle \-p, but don't interpret unlike \-m.
.It Fl d
Deterministic mode; ensures unvarying script output from same input.
This only affects the handling of scripts which use specific features.
.It Fl e
Evaluate strings instead of files. Applies to scripts after.
.It Fl h
Print help for topic, or usage information and a list of topics if none.
.It Fl m
Muted; always disable system audio output.
.It Fl \-mono
Downmix and output audio as mono; this applies to all outputs.
.It Fl o
Write a 16-bit PCM WAV file, always using the sample rate requested.
Or for AU over stdout, "-". Disables system audio output by default.
(WAV can't portably be used over stdout due to a format limitation.)
.It Fl p
Print info for scripts read.
Normally to stdout, but \-\-stdout or \-o\- reserves it for audio,
in which case all printing is to stderr.
.It Fl r
Sample rate in Hz (default 96000);
if unsupported for system audio, warns and prints rate used instead.
.It Fl \-stdout
Send a raw 16-bit output to stdout, always using the sample rate requested.
Reserves stdout for audio; all text printing will be to stderr.
Can't be used together with \-o \-. Doesn't disable any other audio output.
.It Fl v
Be verbose.
Mentions every script processed.
.It Fl V
Print version.
.It Ar variable\| Ns Cm \&= Ns Ar value
Set variable, passed on to scripts as "$variable".
Each value must be a number, with or without a decimal point.
A script may expect, optionally use, or ignore a passed value.
.Pp
The following built-in magic variables have special handling.
.Bl -tag -width Ds
.It Va $seed
Set to reset the rand() value sequence.
.El
.El
.Sh ENVIRONMENT
.Bl -tag -width OSS_AUDIODEV
.It Ev AUDIODEV
Can be set to change the system audio device for
.Nm
to open when using the system audio API it was built to use. (If set to an
empty string or to
.Dq default ,
then the built-in default name will be used
unchanged.) Example values are
.Dq /dev/dsp
for most OSS variants or
.Dq default
for ALSA.
.It Ev AUDIODEVICE
For compatibility: For sndio, is checked and handled by its API when
.Ev AUDIODEV is unset, empty, or set to
.Dq default .
.It Ev OSS_AUDIODEV
For compatibility: For OSS, is checked as a fallback option when
.Ev AUDIODEV
is unset, empty, or set to
.Dq default .
.El
.Sh EXIT STATUS
.Nm
exits with 0 if no scripts are to be processed,
or upon successful processing of one or more scripts,
even if some scripts were excluded due to failed parse or checks.
.Pp
If scripts were to be processed but none of them passed checks for further use,
or if any errors occured after checks (during interpretation and audio output),
1 is returned.
.Sh SAU LANGUAGE
A compact SAU language reference (plaintext) comes with the installation.
By default, it is copied to:
.Pa /usr/local/share/doc/saugns/README.SAU
.Pp
A less compact and more how-to overview can be found at:
.Pa https://sau.frama.io/language.html
.Sh EXAMPLES
One-second beep:
.Dl % "saugns -e ""Wsin"""
.Pp
10 seconds of "engine rumble" using PM:
.Dl % "saugns -e ""Wsin f137 t10 p[Wsin f10*pi p[Wsin r(4/3)(pi/3)]]"""
.Pp
A set of example scripts come with the installation.
By default, they are copied to one of the following locations, depending on whether or not the system seems to house example files under "share/examples/":
.Pp
.Pa /usr/local/share/examples/saugns/
.Pa /usr/local/share/saugns/examples/
.Sh HISTORY
The program was first written in 2011, released in 2012.
Reworked after 2017, renamed from sgensys to saugns in 2019.
More detailed history can be found at:
.Pa https://sau.frama.io/history.html
.Sh AUTHORS
.An Joel K. Pettersson <joelkp@tuta.io>
